<?php

/**
 * @file
 * Common functions for cancel button.
 */

/**
 * Get settings for cancel button.
 * 
 * @param string $name
 *   (optional) setting name.
 * 
 * @return mixed
 *   settings value.
 */
function cancel_button_settings_get($name = NULL) {
  $settings = variable_get('cancel_button_settings');
  
  if(!isset($settings[$name])) {
    return $settings;
  }
  
  return $settings[$name];
}

/**
 * Set settings for cancel button.
 * 
 * @param string $name
 *   setting name.
 * @param (mixed) $value
 *   setting value.
 */
function cancel_button_settings_set($name, $value) {
  $settings = variable_get('cancel_button_settings');
  
  $settings[$name] = $value;
  
  variable_set('cancel_button_settings', $settings);
}

/**
 * Delete setting from cancel button.
 * 
 * @param string $name
 *   (optional) setting name. If not set removes all the settings.
 * @param (mixed) $value
 *   setting value.
 */
function cancel_button_settings_del($name = NULL) {
  
  if(!$name) {
    variable_del('cancel_button_settings');
    return;
  }
  
  $settings = variable_get('cancel_button_settings');
  
  unset($settings[$name]);
  
  variable_set('cancel_button_settings', $settings);
}

/**
 * Checks is form needs cancel button.
 * 
 * @param string $form_id
 *   Form id.
 * @param array $form
 *   Form array.
 * 
 * @return boolean
 *   TRUE is form needs cancel button.
 */
function cancel_buttom_needed($form_id, $form) {
  if (cancel_button_needed_for_user_form($form_id, $form) || cancel_button_needed_for_node_form($form_id, $form)) {
    return TRUE;
  }
}

/**
 * Checks is user form needs cancel button.
 * 
 * @param string $form_id
 *   Form id.
 * @param array $form
 *   Form array.
 * 
 * @return boolean
 *   TRUE is form needs cancel button.
 */
function cancel_button_needed_for_user_form($form_id, $form) {
  if ($form_id == 'user_profile_form') {
    $settings = cancel_button_settings_get('user');
    if (!empty($settings['user'])) {
      return TRUE;
    }
  }
}

/**
 * Checks is node form needs cancel button.
 * 
 * @param string $form_id
 *   Form id.
 * @param array $form
 *   Form array.
 * 
 * @return boolean
 *   TRUE is form needs cancel button.
 */
function cancel_button_needed_for_node_form($form_id, $form) {
  $node_type = '';
  if (isset($form['#node']->type) && isset($form['#node_edit_form'])) {
    $node_type = $form['#node']->type;
  }
  elseif (isset($form['type']) && !empty($form['#type']['#value']) && isset($form['#node_edit_form'])) {
    $node_type = $form['#type']['#value'];
  }
  if (!$node_type) {
    return;
  }
  $settings = cancel_button_settings_get('node');
  if (empty($settings[$node_type])) {
    return;
  }
  if ($form_id == $node_type . '_node_form') {
    return TRUE;
  }
}
