<?php

/**
 * @file
 * Admin settings for cancel button.
 */

/**
 * Callback for cancel button settings.
 */
function _cancel_button_settings($form, &$form_state) {

  $types = node_type_get_names();
  
  $form['node'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#description' => t('Enable cancel button for checked content types'),
    '#options' => $types,
    '#default_value' => cancel_button_settings_get('node'),
  );
  
  $form['user'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User form'),
    '#description' => t('Enable cancel button for user form'),
    '#options' => array(
      'user' => t('Enable for User form'),
    ),
    '#default_value' => cancel_button_settings_get('user'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Callback for cancel button settings submit.
 */
function _cancel_button_settings_submit($form, $form_state) {
  
  // Clear settings before save.
  cancel_button_settings_del();
  
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);
  
  foreach ($form_state['values'] as $key => $value) {
    
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    
    cancel_button_settings_set($key, $value);
  }

  drupal_set_message(t('Cancel button settings have been saved.'));
}
